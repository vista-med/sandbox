package com.ivolodin.vista.repository;

import com.ivolodin.vista.model.entity.EventEntity;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Flux;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface EventRepository extends R2dbcRepository<EventEntity, Long> {

    @Query("SELECT * from Event e where e.eventType_id = 29 and cast(setDate as DATE) = cast(:day as DATE)")
    Flux<EventEntity> findEventEntitiesByContext(LocalDate day);

}
