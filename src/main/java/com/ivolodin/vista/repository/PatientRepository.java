package com.ivolodin.vista.repository;

import com.ivolodin.vista.model.entity.PatientEntity;
import org.springframework.data.r2dbc.repository.R2dbcRepository;

public interface PatientRepository extends R2dbcRepository<PatientEntity, Long> {
}
