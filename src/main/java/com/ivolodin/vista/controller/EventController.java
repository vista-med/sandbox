package com.ivolodin.vista.controller;

import com.ivolodin.vista.model.dto.EventDto;
import com.ivolodin.vista.model.entity.EventEntity;
import com.ivolodin.vista.repository.EventRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@RestController
public class EventController {
    private final EventRepository eventRepository;
    @GetMapping("/event/{id}")
    public Mono<EventDto> getById(@PathVariable("id") String id){
        return eventRepository.findById(Long.parseLong(id))
                .flatMap(this::convertToDto);
    }

    private Mono<EventDto> convertToDto(EventEntity eventEntity) {
        return Mono.fromCallable(() -> new EventDto(eventEntity.getId(), eventEntity.getEventType()));
    }
}
