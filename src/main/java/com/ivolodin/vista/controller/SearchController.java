package com.ivolodin.vista.controller;

import com.ivolodin.vista.model.dto.request.SearchRequestContextDto;
import com.ivolodin.vista.model.dto.response.PatientResponseDto;
import com.ivolodin.vista.service.SearchService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

@RequiredArgsConstructor
@RestController
@RequestMapping("/search")
public class SearchController {
    private final SearchService searchService;
    @PostMapping
    Flux<PatientResponseDto> getPatientStreamByDoctorAndTime(@RequestBody SearchRequestContextDto searchRequestContextDto) {
        return searchService.searchPatient(searchRequestContextDto);
    }
}
