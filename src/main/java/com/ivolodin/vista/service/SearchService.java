package com.ivolodin.vista.service;

import com.ivolodin.vista.model.dto.request.SearchRequestContextDto;
import com.ivolodin.vista.model.dto.response.PatientResponseDto;
import com.ivolodin.vista.model.entity.EventEntity;
import com.ivolodin.vista.model.entity.PatientEntity;
import com.ivolodin.vista.repository.EventRepository;
import com.ivolodin.vista.repository.PatientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Component
public class SearchService {
    private final PatientRepository patientRepository;
    private final EventRepository eventRepository;

    public Flux<PatientResponseDto> searchPatient(SearchRequestContextDto requestContextDto) {
        var eventsFlux = eventRepository.findEventEntitiesByContext(requestContextDto.dateToCome()).log();
        var patientEntityMono = patientRepository.findById(eventsFlux.map(EventEntity::getClientId)).log();

        return eventsFlux.map(eventEntity -> patientEntityMono.flatMapMany(patientEntity -> buildResponseDto(eventEntity, patientEntity)))
                .flatMap(Flux::concat);
    }

    private Mono<PatientResponseDto> buildResponseDto(EventEntity eventEntity, PatientEntity patientEntity) {
        return Mono.fromCallable(() -> PatientResponseDto.builder()
                .arrivingTime(eventEntity.getDate())
                .lastName(patientEntity.getLastName())
                .firstName(patientEntity.getFirstName())
                .patronymicName(patientEntity.getPatrName())
                .build());
    }

}
