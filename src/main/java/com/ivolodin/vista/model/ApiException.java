package com.ivolodin.vista.model;

public class ApiException extends RuntimeException {
    public ApiException(String message) {
        super(message);
    }
}
