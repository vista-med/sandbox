package com.ivolodin.vista.model.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@Data
@Table("Event")
public class EventEntity {
    @Id
    private Long id;

    @Column("eventType_id")
    private Integer eventType;

    @Column("setDate")
    private LocalDateTime date;

    @Column("client_id")
    private Long clientId;

    @Column("setPerson_id")
    private Long doctorId;
}