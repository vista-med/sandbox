package com.ivolodin.vista.model.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Table("Client")
public class PatientEntity {
    @Id
    private Long id;
    @Column("lastName")
    private String lastName;

    @Column("firstName")
    private String firstName;

    @Column("patrName")
    private String patrName;

}
