package com.ivolodin.vista.model.dto.request;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public record SearchRequestContextDto(@NotNull LocalDate dateToCome,
                                      @NotNull String doctorId,
                                      @NotNull String patientId) {
}