package com.ivolodin.vista.model.dto;

public record EventDto(long id, long eventType){
}
